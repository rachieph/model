//
//  SysMessage.swift
//  Model
//
//  Created by IT on 27/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

public class SysMessage: NSObject, Codable {
    public var content = ""
    public var noticeId = "";
    public var status = "";
    public var time = "";
    public var title = "";
}
