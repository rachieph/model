//
//  RecommendGift.swift
//  Model
//
//  Created by IT on 27/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

/// <#Description#>
public class RecommendGift: NSObject, Codable {
    
    /// 账号
    public var account = ""
    
    /// 礼金
    public var amount = ""
    
    /// 时间
    public var dateTime = ""
    
    /// 积分
    public var point = ""
    
    /// 1 积分 2 礼金
    public var type = ""
}
