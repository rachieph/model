//
//  GiftAndAmtDetail.swift
//  Model
//
//  Created by IT on 29/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

public class GiftAndAmtDetail: NSObject, Codable {
    public var platform = ""
    public var amount = ""
    public var date = ""
    public var Id = ""
}
