//
//  GiftAndAmtDetails.swift
//  Model
//
//  Created by IT on 29/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

/// signal
public class GiftAndAmtDetails: NSObject, Codable {
    public var dataList = [GiftAndAmtDetail]()
}


/// multi
public class GiftAndAmtMultiDetails: NSObject, Codable {
    public var platform = ""
    public var dataDetail = [GiftAndAmtDetail]()
}
