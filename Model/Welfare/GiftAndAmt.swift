//
//  GiftAndAmt.swift
//  Model
//
//  Created by IT on 29/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

/// 自助服务区优惠详细内容
public class GiftAndAmt: NSObject, Codable {
    
    /// 显示名称
    public var typeName = ""
    
    /// 金额
    public var amount = ""
    
    /// 是否按平台显示
    public var isPlatform = ""
    
    /// 类型
    public var typeCode = ""
    
    /// 图片地址
    public var image = ""
}
