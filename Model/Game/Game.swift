//
//  Game.swift
//  Model
//
//  Created by IT on 27/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

public class Game: NSObject, Codable {
    public var gameCode = ""
    public var gameNameCn = ""
    public var gameNameEn = ""
    public var imgUrl = ""
    public var isFav = ""
    public var platFormCode = ""
}
