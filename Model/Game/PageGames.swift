//
//  PageGames.swift
//  Model
//
//  Created by IT on 29/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

public class PageGames: NSObject {
    public var hasNext = ""
    public var totalSize = ""
    public var currentPage = ""
    public var data = [Game]()
}

// MARK: - PageProtocol
extension PageGames: PageProtocol {
    public func isFirstPage() -> Bool {
        return currentPage == firstpage
    }
    
    public func isLastPage() -> Bool {
        return hasNext == lastpage
    }
}
