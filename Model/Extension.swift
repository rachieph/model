//
//  Extension.swift
//  Model
//
//  Created by IT on 29/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import Foundation


extension KeyedDecodingContainer {
    public func decodeString(forKey key: KeyedDecodingContainer<K>.Key, value: String) -> String {
        if let value = try? self.decode(String.self, forKey: key) {
            return value;
        } else {
            return value;
        }
    }

    
    
}
