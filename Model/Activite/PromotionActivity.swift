//
//  PromotionActivity.swift
//  Model
//
//  Created by IT on 27/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

public class PromotionActivity: NSObject, Codable {
    public var activityUrl = ""
    public var logoUrl = ""
    public var tags = ""
    public var title = ""
}
