//
//  PromotionActivityTags.swift
//  Model
//
//  Created by IT on 27/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

public class PromotionActivityTags: NSObject, Codable {
    
    public var promotion = [PromotionActivity]()
    
    public var tagSet = [String]()
    
    public var selectedIndex = 0
}
