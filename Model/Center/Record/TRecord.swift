//
//  TRecord.swift
//  Model
//
//  Created by IT on 29/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

/// 转账记录
public class TRecord: NSObject, Codable {
    public var type = ""
    public var amount = ""
    public var status = ""
    public var createdTime = ""
    public var seqNo = ""
    public var notes = ""
}

public class PageTRecords: NSObject, Codable {
    public var hasNext = ""
    public var totalSize = ""
    public var currentPage = ""
    public var data = [TRecord]()
}

// MARK: - PageProtocol
extension PageTRecords: PageProtocol {
    public func isFirstPage() -> Bool {
        return currentPage == firstpage
    }
    
    public func isLastPage() -> Bool {
        return hasNext == lastpage
    }
}
