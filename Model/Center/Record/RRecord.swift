//
//  RRecord.swift
//  Model
//
//  Created by IT on 29/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

/// 推荐礼金
public class RRecord: NSObject, Codable {
    public var num = ""
    public var type = ""
    public var account = ""
    public var status = ""
    public var createdTime = ""
    public var seqNo = ""
}

public class PageRRecords: NSObject, Codable {
    public var hasNext = ""
    public var totalSize = ""
    public var currentPage = ""
    public var data = [RRecord]()
}

// MARK: - PageProtocol
extension PageRRecords: PageProtocol {
    public func isFirstPage() -> Bool {
        return currentPage == firstpage
    }
    
    public func isLastPage() -> Bool {
        return hasNext == lastpage
    }
}
