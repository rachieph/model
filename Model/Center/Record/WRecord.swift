//
//  WRecord.swift
//  Model
//
//  Created by IT on 29/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

/// 取款记录
public class WRecord: NSObject, Codable {
    public var cardNo = ""
    public var amount = ""
    public var status = ""
    public var createdTime = ""
    public var seqNo = ""
    public var notes = ""
    public var bankName = ""
}

public class PageWRecords: NSObject, Codable {
    public var hasNext = ""
    public var totalSize = ""
    public var currentPage = ""
    public var data = [WRecord]()
}

// MARK: - PageProtocol
extension PageWRecords: PageProtocol {
    public func isFirstPage() -> Bool {
        return currentPage == firstpage
    }
    
    public func isLastPage() -> Bool {
        return hasNext == lastpage
    }
}
