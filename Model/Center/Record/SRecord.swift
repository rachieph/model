//
//  SRecord.swift
//  Model
//
//  Created by IT on 29/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

/// 积分纪录
public class SRecord: NSObject, Codable {
    public var points = ""
    public var status = ""
    public var createdTime = ""
    public var seqNo = ""
}


public class PageSRecords: NSObject, Codable {
    public var hasNext = ""
    public var totalSize = ""
    public var currentPage = ""
    public var data = [SRecord]()
}

// MARK: - PageProtocol
extension PageSRecords: PageProtocol {
    public func isFirstPage() -> Bool {
        return currentPage == firstpage
    }
    
    public func isLastPage() -> Bool {
        return hasNext == lastpage
    }
}
