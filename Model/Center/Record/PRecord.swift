//
//  PRecord.swift
//  Model
//
//  Created by IT on 29/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

/// 优惠记录
public class PRecord: NSObject, Codable {
    public var amount = ""
    public var status = ""
    public var createdTime = ""
    public var seqNo = ""
    public var notes = ""
}

public class PagePRecords: NSObject, Codable {
    public var hasNext = ""
    public var totalSize = ""
    public var currentPage = ""
    public var data = [PRecord]()
}

// MARK: - PageProtocol
extension PagePRecords: PageProtocol {
    public func isFirstPage() -> Bool {
        return currentPage == firstpage
    }
    
    public func isLastPage() -> Bool {
        return hasNext == lastpage
    }
}
