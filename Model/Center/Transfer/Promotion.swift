//
//  Promotion.swift
//  Model
//
//  Created by IT on 29/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

public class Promotion: NSObject, Codable {
    
    /// code
    public var promotionCode = ""
    
    /// 描述
    public var promotionRemark = ""
}
