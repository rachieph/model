//
//  Channel.swift
//  Model
//
//  Created by IT on 29/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

public class Channel: NSObject, Codable {
    public var channelName = ""
    public var fixedAmounts = [Int]()
    public var id = ""
    public var ifType = ""
    public var maxAmount = ""
    public var minAmount = ""
    public var number = ""
    
    
    
    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        channelName = values.decodeString(forKey: Channel.CodingKeys.channelName, value: "")
        id = values.decodeString(forKey: .id, value: "")
        if let fixAmount = try? values.decode([Int].self, forKey: .fixedAmounts) {
            fixedAmounts = fixAmount
        }
        
        ifType = values.decodeString(forKey: .ifType, value: "")
        maxAmount = values.decodeString(forKey: .maxAmount, value: "")
        minAmount = values.decodeString(forKey: .minAmount, value: "")
        number = values.decodeString(forKey: .number, value: "")
    }
    
    
}
