//
//  Transfer.swift
//  Model
//
//  Created by IT on 29/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

public class Transfer: NSObject, Codable {
    
    /// 转出
    public var transferFrom = ""
    
    /// 转入列表
    public var transferTo = [TransferTo]()
}
