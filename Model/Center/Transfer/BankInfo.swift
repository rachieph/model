//
//  BankInfo.swift
//  Model
//
//  Created by IT on 29/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

public class BankInfo: NSObject, Codable {
    public var bankAddr = ""
    public var bankCode = ""
    public var bankName = ""
    public var cardNo = ""
    public var tagId = ""
    public var tagName = ""
}
