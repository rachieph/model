//
//  PageProtocol.swift
//  Model
//
//  Created by IT on 27/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import Foundation

/// Page 协议
public protocol PageProtocol {
    
    /// 是否是第一页
    ///
    /// - Returns: 是否是开始页
    func isFirstPage() -> Bool
    
    /// 是否是最后页
    ///
    /// - Returns: 是否是最终页
    func isLastPage() -> Bool
}


/// 是否为最终页
public let lastpage = "0"

/// 是否为第一页
public let firstpage = "1"
