//
//  PlatformBalance.swift
//  Model
//
//  Created by IT on 29/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit

public class PlatformBalance: NSObject, Codable {
    public var balance = ""
    public var gamePlatformCode = ""
}
